package com.xteam.discountasciiwarehouse.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.xteam.discountasciiwarehouse.dao.ProductDao;
import com.xteam.discountasciiwarehouse.dao.PurchaseDao;
import com.xteam.discountasciiwarehouse.dao.UserDao;
import com.xteam.discountasciiwarehouse.dao.exception.ProductNotFoundException;
import com.xteam.discountasciiwarehouse.dao.exception.UserNotFoundException;
import com.xteam.discountasciiwarehouse.domain.PopularPurchase;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;
import com.xteam.discountasciiwarehouse.domain.builder.ProductBuilder;
import com.xteam.discountasciiwarehouse.domain.builder.PurchaseBuilder;
import com.xteam.discountasciiwarehouse.domain.builder.UserBuilder;
import com.xteam.discountasciiwarehouse.service.impl.PurchaseServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceTest {

	@Mock
	PurchaseDao purchaseRepository;

	@Mock
	UserDao userRepository;

	@Mock
	ProductDao productDao;

	@InjectMocks
	PurchaseService purchaseService = new PurchaseServiceImpl();

	@Before
	public void setUp() throws ProductNotFoundException {}

	@Test
	public void listPopularPurchaseStructure() throws UserNotFoundException, ProductNotFoundException {
		List<Purchase> purchases = PurchaseBuilder.aList(5);
		for (Purchase purchase : purchases) { 
			when(productDao.getById(purchase.getProduct().getId())).thenReturn(ProductBuilder.aProduct().withId(purchase.getId()).build());
			when(purchaseRepository.listByProduct(purchase.getProduct().getId())).thenReturn(PurchaseBuilder.aList(1));
		}

		when(purchaseRepository.listByUsername("xuser1", 5)).thenReturn(purchases);
		when(userRepository.getByUsername("xuser1")).thenReturn(UserBuilder.aUser().withUsername("xuser1").build());
		when(productDao.getById(101)).thenReturn(ProductBuilder.aProduct().withId(101).build());

		PopularPurchase purchase = purchaseService.listPopular("xuser1", 5).get(1);
	
		assertTrue(purchase.getId()>0);
		assertTrue(!purchase.getProduct().getFace().isEmpty());
		assertTrue(purchase.getProduct().getPrice()>0);
		assertTrue(purchase.getProduct().getSize()>0);
		assertFalse(purchase.getUsers().isEmpty());
	}

	@Test(expected=UserNotFoundException.class)
	public void listPopularPurchasesWithNonExistentNonExistentUser() throws UserNotFoundException {
		when(userRepository.getByUsername("invalidusername")).thenReturn(new User());
		purchaseService.listPopular("invalidusername", 5);
	}

	@Test
	public void moreThanOnePurchaseForTheSameProduct() throws ProductNotFoundException, UserNotFoundException {
		User user1 = UserBuilder.aUser().withUsername("xuser1").build();
		User user2 = UserBuilder.aUser().withUsername("xuser2").build();
		Product product = ProductBuilder.aProduct().withId(101).build();
		
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases.add(PurchaseBuilder.aPurchase().withProduct(product).withUser(user1).build());
		purchases.add(PurchaseBuilder.aPurchase().withProduct(product).withUser(user1).build());
		purchases.add(PurchaseBuilder.aPurchase().withProduct(product).withUser(user2).build());
		
		when(userRepository.getByUsername("xuser1")).thenReturn(user1);
		when(userRepository.getByUsername("xuser2")).thenReturn(user2);
		when(purchaseRepository.listByUsername("xuser1", 5)).thenReturn(purchases);
		when(productDao.getById(101)).thenReturn(ProductBuilder.aProduct().withId(101).build());
		
		assertEquals(1, purchaseService.listPopular("xuser1", 5).size());
	}
	
	@Test
	public void purchaseWithInvalidProduct() throws ProductNotFoundException, UserNotFoundException {
		User user1 = UserBuilder.aUser().withUsername("xuser1").build();
		User user2 = UserBuilder.aUser().withUsername("xuser2").build();

		Product product = ProductBuilder.aProduct().withId(510).build();
	
		List<Purchase> purchase = new ArrayList<Purchase>();
		purchase.add(PurchaseBuilder.aPurchase().withProduct(product).withUser(user1).build());
		purchase.add(PurchaseBuilder.aPurchase().withProduct(product).withUser(user2).build());

		when(purchaseRepository.listByUsername(user1.getUsername(), 5)).thenReturn(purchase);
		when(productDao.getById(510)).thenThrow(new ProductNotFoundException());
		when(userRepository.getByUsername(user1.getUsername())).thenReturn(user1);
		
		assertTrue(purchaseService.listPopular(user1.getUsername(), 5).isEmpty());
	}
	
}
