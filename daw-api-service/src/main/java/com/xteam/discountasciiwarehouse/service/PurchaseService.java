package com.xteam.discountasciiwarehouse.service;

import java.util.List;

import com.xteam.discountasciiwarehouse.dao.exception.UserNotFoundException;
import com.xteam.discountasciiwarehouse.domain.PopularPurchase;

/**
 * Services related to the Discount Ascii Warehouse purchases
 *
 * @author Wagner renzi
 * @see PopularPurchase
 */
public interface PurchaseService {
	
	/**
	 * List popular purchases of a given user. Popular purchases refer to
	 * those that has more people who bought the same product the given user.  
	 * @param username
	 * @param limit
	 * @throws UserNotFoundException if the username doesn't exist
	 */	
	List<PopularPurchase> listPopular(String username, int limit) throws UserNotFoundException;
		
}
