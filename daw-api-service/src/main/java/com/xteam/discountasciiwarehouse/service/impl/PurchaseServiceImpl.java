package com.xteam.discountasciiwarehouse.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xteam.discountasciiwarehouse.dao.ProductDao;
import com.xteam.discountasciiwarehouse.dao.PurchaseDao;
import com.xteam.discountasciiwarehouse.dao.UserDao;
import com.xteam.discountasciiwarehouse.dao.exception.ProductNotFoundException;
import com.xteam.discountasciiwarehouse.dao.exception.UserNotFoundException;
import com.xteam.discountasciiwarehouse.domain.PopularPurchase;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;
import com.xteam.discountasciiwarehouse.service.PurchaseService;

@Component
public class PurchaseServiceImpl implements PurchaseService {

	Logger logger = LoggerFactory.getLogger(PurchaseServiceImpl.class);
	
	@Autowired
	PurchaseDao purchaseDao;

	@Autowired
	UserDao userDao;

	@Autowired
	ProductDao productDao;
	
	@Override
	public List<PopularPurchase> listPopular(String username, int limit) throws UserNotFoundException {
		
		List<Purchase> userPurchases = purchaseDao.listByUsername(username, limit)
					.stream()
					.collect(Collectors.toCollection(() -> new TreeSet<Purchase>((p1,p2)->p2.getProduct().getId().compareTo(p1.getProduct().getId()))))
					.stream()
					.collect(Collectors.toList());
		
		if (userPurchases.isEmpty()) {
			if (userDao.getByUsername(username).getUsername()==null) {
				throw new UserNotFoundException(String.join("", "User with username of '", username, "' was not found"));
			}
		}

		List<PopularPurchase> popularPurchases = new ArrayList<PopularPurchase>();
		
		for (Purchase userPurchase : userPurchases) {

			Product product;
			try {
				product = productDao.getById(userPurchase.getProduct().getId());
			} catch (ProductNotFoundException e) {
				logger.error("Product with id '{}' not found",userPurchase.getProduct().getId());
				continue;
			}

			PopularPurchase popularPurchase = new PopularPurchase();
			popularPurchase.setProduct(product);
			popularPurchase.setId(userPurchase.getId());

			Set<User> users = new TreeSet<User>((u1,u2)->u1.getUsername().compareTo(u2.getUsername()));
			for (Purchase purchasesByProduct : purchaseDao.listByProduct(userPurchase.getProduct().getId())) {
				users.add(purchasesByProduct.getUser());
			}
			
			popularPurchase.setUsers(users);
			
			popularPurchases.add(popularPurchase);
			
		}
		
		return popularPurchases
				.stream()
				.sorted((p1, p2) -> Integer.compare(p2.getUsers().size(), p1.getUsers().size()))
				.collect(Collectors.toList());

	}
	
}
