# Application Scope
	
## Requirements
For this application, the scope is to build a backend for a new feature of the Discount Ascii Warehouse ecommerce platform.
The application must produce a list of "Popular purchases", so customers can see who else bought the same products as them.  The application will need to accept HTTP requests to `/api/recent_purchases/:username` and respond with a list of recently purchased products, and the names of other users who recently purchased them.

The application must cache API request `GET /api/products/:product_idts` so that it can respond as quickly as possible.

If a username is provided and it cannot be found, the API should respond with `"User with username of '{{username}}' was not found"`

Data about users, products and purchases is available to you via an external API: 
https://github.com/x-team/daw-purchases/blob/master/README.md#api-reference


## Definitions I had with the X-Team

- If there is no other buyer for some of the products, the service will return all recently purchased products anyway.

- If one of the external DAW API services not respond (timeout, unavailable, etc), the service will try to use data from cache. But if that's also unavailable, the service send a response to show that the resource is temporarily unavailable

- This service only needs to use use HTTP error codes

- At this moment it's not necessary any security requirement (authentication, SSL, etc). If necessary, this service is prepared to be configured with a great security layer. See http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-security.html

- The user is not allowed to update its username

- No need to enable CORS

- No need to apply HATEOAS. If necessary, it will be easy to apply: https://spring.io/guides/gs/rest-hateoas/

- If the user bought the same product twice, the service must consider only one purchase

- It's not necessary to add version for the API in the URI

------------------------------------------------

# Project Modules

This project is divided in four modules:


## REST
The presentation layer responsible for the delivery of the RESTFul information returned by the service layer

## Service
It's the layer where all the business logic is placed. In this case, there is the service that knows how to request and transform data from the external DAW API into the response data expected by X-Team consumer application.

## DAO
The data access layer which provides simplified access to data stored in the external DAW API

## Domain
This is the domain layer, which is a collection of entity objects. The major scope of this layer is to create a standardized set of objects, that could be potentially reused within different projects.


------------------------------------------------

# Software Architecture

I used Spring Framework (+Spring Boot) because it’s a very stable library and it has many integrations out-of-the-box. It has a nice Injection Dependency & Context container. Because of it, Spring Boot applications do not require an application server that fully supports the Java EE specification. You can run it with an embedded web server like Jetty or Tomcat. Besides of that, Spring Framework fully supports the JCache standard annotations (JSR 107). Spring Boot favors convention over configuration and is designed to get you up and running as quickly as possible. 

For the presentation layer, I used Spring MVC, which is a handy module to expose RESTFul web-services. Basically, you just have to annotate your classes and methods with the `@RestController` and `@RequestMapping`.  I also could use Jersey or RESTEasy, but I have worked with Spring MVC in my company for a long time and it's fantastic. In the next days I'll build a proof of concept with these two other frameworks, which fully supports the JAX-RS (JSR-339). 

There are a bunch of good libraries and servers to be used as the Cache tool: Redis, Infinispan, Guava, EhCache, Memcached and etc. As this application will not be behind a load balancer, I prefered to use the library Guava (by Google). It has a very fast in-memory cache module. If you need to put it behind a load balancer, it will be really simple to configure the Cache Manager to use a Redis cache server. The Guava caches the main application request `GET /api/products/:product_idts` and, besides, It also caches two requests that are made to external DAW API: `/purchases/by_product/:productId` and `/purchases/by_product/:productId`. For analysis purpose, I've set all of them to last two minutes.

The four external DAW API services are being consumed by the Spring Web Client. It applies the Jackson JSON processing library under the hood. If any of the services is unavailable, an error is thrown. 

In my humble opinion, the structure of the JSON defined are good, but they are not so well defined. With this in mind, I have abstracted this external model for this application. My approach is model a clean entity domain and then I use Dozer to translate external domains to my own one. 


------------------------------------------------

# Setup and Installation

You have two ways to start the application:

* Running the `daw-api-rest-1.0.0.jar` with java command, or...
* Downloading the source, compile it, and then run it with `mvn spring-boot:run`

## Run the official Jar

### Software requirements
* Java 8

### Run DAW API with java command
Download the daw-api-rest-1.0-0.jar [e4307c917d43ea663684a0d5d79ac9b1] file into a folder, then run it!
```ssh
mkdir /my/apps/folder/ -p
wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=0B0_YhHD5LkEIOFlYMTAyNVdidjQ' -O daw-api-rest-1.0.0.jar
java -jar /my/apps/folder/daw-api-rest-1.0.0.jar
```
Test it!
```ssh
curl http://localhost:8080/api/recent_purchases/Kade6?limit=5
```


## Download the source, compile it, and run it!

### Software requirements
* Java 8
* Maven 3

### Run DAW API with java command
Download the source gz file file into a folder:
```ssh 	
mkdir /my/apps/folder/ -p
wget --no-check-certificate 'https://drive.google.com/open?id=0B0_YhHD5LkEITlNFVmZYX1U4UHM' -O daw-api-rest-1.0.0.jar
```

Compile and install 
```ssh
cd /my/apps/folder/
mvn clean install
mvn spring-boot:run
```
Test it!
```ssh
curl http://localhost:8080/api/recent_purchases/Kade6?limit=5
```