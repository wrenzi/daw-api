package com.xteam.discountasciiwarehouse.dao.mapper;

import static org.junit.Assert.assertEquals;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.xteam.discountasciiwarehouse.dao.config.DozerConfig;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.PurchaseDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.UserDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder.ProductResponseDAWResourceBuilder;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder.PurchaseDAWResourceBuilder;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder.UserDAWResourceBuilder;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;

@RunWith(MockitoJUnitRunner.class)
public class DozerMapperTest {

	@Test
	public void productMarshalling() {
		ProductDAWResource source = ProductResponseDAWResourceBuilder.aProductResponse().build().getProduct();
		Product destination = new Product();
		DozerBeanMapper mapper = new DozerConfig().beanMapper();
		mapper.map(source, destination);
		
		assertEquals(source.getId(), destination.getId().intValue());
		assertEquals(source.getFace(), destination.getFace());
		assertEquals(source.getPrice(), destination.getPrice().intValue());
		assertEquals(source.getSize(), destination.getSize().intValue());
	}
	
	@Test
	public void userMarshalling() {
		UserDAWResource source = UserDAWResourceBuilder.aUser().build();
		User destination = new User();
		DozerBeanMapper mapper = new DozerConfig().beanMapper();
		mapper.map(source, destination);
		
		assertEquals(source.getEmail(), destination.getEmail());
		assertEquals(source.getUsername(), destination.getUsername());
	}

	@Test
	public void purchaseMarshilling() {
		PurchaseDAWResource source = PurchaseDAWResourceBuilder.aPurchase().build();
		Purchase destination = new Purchase();
		DozerBeanMapper mapper = new DozerConfig().beanMapper();
		mapper.map(source, destination);

		assertEquals(source.getId(), destination.getId().intValue());
		assertEquals(source.getDate(), destination.getDate());
		assertEquals(source.getProductId(), destination.getProduct().getId().intValue());
		assertEquals(source.getUsername(), destination.getUser().getUsername());
	}
	
}
