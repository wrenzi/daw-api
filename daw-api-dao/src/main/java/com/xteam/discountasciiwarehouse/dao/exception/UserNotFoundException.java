package com.xteam.discountasciiwarehouse.dao.exception;

public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = -5809313449147965708L;
	
	public UserNotFoundException(){}
	
	public UserNotFoundException(String message) {
		super(message);
	}

}
