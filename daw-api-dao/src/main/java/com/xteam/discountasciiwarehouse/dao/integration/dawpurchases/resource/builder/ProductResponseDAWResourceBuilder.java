package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductResponseDAWResource;

public class ProductResponseDAWResourceBuilder {

	private ProductDAWResource product;
	
    private ProductResponseDAWResourceBuilder() {}

	public static ProductResponseDAWResourceBuilder aProductResponse() {
		ProductResponseDAWResourceBuilder builder = new ProductResponseDAWResourceBuilder();
		builder.withProduct(ProductDAWResourceBuilder.aProduct().build());
		return builder;
	}
    
    public ProductResponseDAWResourceBuilder withProduct(ProductDAWResource product) {
        this.product = product;
        return this;
    }

    public ProductResponseDAWResource build() {
        return new ProductResponseDAWResource(this.product);
    }

}