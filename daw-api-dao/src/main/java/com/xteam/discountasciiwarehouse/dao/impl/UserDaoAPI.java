package com.xteam.discountasciiwarehouse.dao.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.xteam.discountasciiwarehouse.dao.UserDao;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.UserDAWResource;
import com.xteam.discountasciiwarehouse.domain.User;

@Repository("userRepository")
public class UserDaoAPI implements UserDao {

	@Value("${daw.user.getbyusername.url}")
	private String getByUsernameUrl;
	
    @Autowired
    private DozerBeanMapper mapper;

    @Override
	public User getByUsername(String username) {
		UserDAWResource user = new RestTemplate().getForObject(this.getByUsernameUrl, UserDAWResource.class, username);
		return mapper.map(user, User.class);
	}
	
}
