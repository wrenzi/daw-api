package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.PurchaseDAWResource;

public class PurchaseDAWResourceBuilder {
	
	private int id; 
	
	private String username;
	
	private int productId;
	
	private Date date;	
	
    private PurchaseDAWResourceBuilder() {}

	public static PurchaseDAWResourceBuilder aPurchase() {
		Random rand = new Random();
		PurchaseDAWResourceBuilder builder = new PurchaseDAWResourceBuilder();
		builder
			.withId(rand.nextInt(1000))
			.withUsername("user"+rand.nextInt(100))
			.withProductId(rand.nextInt(1000))
			.withDate(Calendar.getInstance().getTime());
		return builder;
	}
    
    public PurchaseDAWResourceBuilder withId(int id) {
        this.id = id;
        return this;
    }

    public PurchaseDAWResourceBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public PurchaseDAWResourceBuilder withProductId(int productId) {
        this.productId = productId;
        return this;
    }

    public PurchaseDAWResourceBuilder withDate(Date date) {
        this.date = date;
        return this;
    }

    public PurchaseDAWResource build() {
        return new PurchaseDAWResource(this.id,this.username,this.productId,this.date);
    }

}