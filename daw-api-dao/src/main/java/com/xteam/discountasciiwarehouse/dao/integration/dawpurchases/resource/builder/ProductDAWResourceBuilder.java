package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder;

import java.util.Random;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;

public class ProductDAWResourceBuilder {

	private int id; 
	
	private String face;
	
	private int price;
	
	private int size;
	
    private ProductDAWResourceBuilder() {}

	public static ProductDAWResourceBuilder aProduct() {
		Random rand = new Random();
		ProductDAWResourceBuilder builder = new ProductDAWResourceBuilder();
		builder
			.withId(rand.nextInt(100))
			.withFace(":)")
			.withPrice(rand.nextInt(200)+1)
			.withSize(rand.nextInt(40)+1);
		return builder;
	}
    
    public ProductDAWResourceBuilder withId(int id) {
        this.id = id;
        return this;
    }

    public ProductDAWResourceBuilder withFace(String face) {
        this.face = face;
        return this;
    }

    public ProductDAWResourceBuilder withPrice(int price) {
        this.price = price;
        return this;
    }

    public ProductDAWResourceBuilder withSize(int size) {
        this.size = size;
        return this;
    }

    public ProductDAWResource build() {
        return new ProductDAWResource(id,face,price,size);
    }

}