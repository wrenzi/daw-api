package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDAWResource implements Serializable {
	
	private static final long serialVersionUID = 6502221144879775193L;

	private int id;
	
	private String face;
	
	private int price;
	
	private int size;

	public ProductDAWResource() {}
	
	public ProductDAWResource(int id, String face, int price, int size) {
		super();
		this.id = id;
		this.face = face;
		this.price = price;
		this.size = size;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "ProductResource [id=" + id + ", face=" + face + ", price=" + price + ", size=" + size + "]";
	}
	
}
