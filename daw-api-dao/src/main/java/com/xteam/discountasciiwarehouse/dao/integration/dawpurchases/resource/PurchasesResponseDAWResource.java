package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchasesResponseDAWResource implements Serializable {
	
	private static final long serialVersionUID = 6739192516230318412L;

	private PurchaseDAWResource[] purchases;

	public PurchasesResponseDAWResource() {}
	
	public PurchasesResponseDAWResource(PurchaseDAWResource[] purchases) {
		this.purchases = purchases;
	}

	public PurchaseDAWResource[] getPurchases() {
		return purchases;
	}

	public void setPurchases(PurchaseDAWResource[] purchases) {
		this.purchases = purchases;
	}

	@Override
	public String toString() {
		return "PurchaseResponse [purchases=" + Arrays.toString(purchases) + "]";
	}

}
