package com.xteam.discountasciiwarehouse.dao;

import com.xteam.discountasciiwarehouse.domain.User;

public interface UserDao {

	User getByUsername(String username);

}
