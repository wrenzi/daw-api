package com.xteam.discountasciiwarehouse.dao;

import java.util.List;

import com.xteam.discountasciiwarehouse.domain.Purchase;

public interface PurchaseDao {

	List<Purchase> listByUsername(String username, int limit);

	List<Purchase> listByProduct(int productId);
	
}
