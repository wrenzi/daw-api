package com.xteam.discountasciiwarehouse.dao.config;

import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.PurchaseDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.UserDAWResource;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;

@Configuration
public class DozerConfig {

    @Bean
    @Primary
    public BeanMappingBuilder beanMappingBuilder() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(ProductDAWResource.class, Product.class);
                mapping(PurchaseDAWResource.class, Purchase.class)
                	.fields("username", "user.username")
                	.fields("productId", "product.id");
                mapping(UserDAWResource.class, User.class);
            }
        };
    }

    @Bean
    public DozerBeanMapper beanMapper() {
        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
        dozerBeanMapper.addMapping(beanMappingBuilder());
        return dozerBeanMapper;
    }

}
