package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseDAWResource implements Serializable {
	
	private static final long serialVersionUID = -6504327155086556472L;

	private int id; 
	
	private String username;
	
	private int productId;
	
	private Date date;

	public PurchaseDAWResource() {}
	
	public PurchaseDAWResource(int id, String username, int productId, Date date) {
		super();
		this.id = id;
		this.username = username;
		this.productId = productId;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Purchase [id=" + id + ", username=" + username + ", productId=" + productId + ", date=" + date + "]";
	}
	
}
