package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDAWResource implements Serializable {
	
	private static final long serialVersionUID = -9076576649618254777L;

	private String username;
	
	private String email;
	
	public UserDAWResource() {}
	
	public UserDAWResource(String username, String email) {
		super();
		this.username = username;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", email=" + email + "]";
	}
	
}
