package com.xteam.discountasciiwarehouse.dao.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.xteam.discountasciiwarehouse.dao.PurchaseDao;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.PurchasesResponseDAWResource;
import com.xteam.discountasciiwarehouse.domain.Purchase;

@Repository("purchaseRepository")
public class PurchaseDaoAPI implements PurchaseDao {

	@Value("${daw.purchase.listbyusername.url}")
	private String listByUsernameUrl;
	
	@Value("${daw.purchase.listbyproduct.url}")
	private String listByProductUrl;

    @Autowired
    private DozerBeanMapper mapper;

	@Override
	public List<Purchase> listByUsername(String username, int limit) {
		RestTemplate restTemplate = new RestTemplate();
		PurchasesResponseDAWResource purchaseResource = restTemplate.getForObject(this.listByUsernameUrl, PurchasesResponseDAWResource.class, username, limit);
		return Arrays.asList(purchaseResource.getPurchases())
				.stream()
				.map(entity -> mapper.map(entity, Purchase.class))
	            .collect(Collectors.toList());
	}
	
	@Override
	@Cacheable("listByProduct")
	public List<Purchase> listByProduct(int productId) {
		RestTemplate restTemplate = new RestTemplate();
		PurchasesResponseDAWResource purchaseResource = restTemplate.getForObject(this.listByProductUrl, PurchasesResponseDAWResource.class, productId);
		return Arrays.asList(purchaseResource.getPurchases())
				.stream()
				.map(entity -> mapper.map(entity, Purchase.class))
	            .collect(Collectors.toList());
	}

}
