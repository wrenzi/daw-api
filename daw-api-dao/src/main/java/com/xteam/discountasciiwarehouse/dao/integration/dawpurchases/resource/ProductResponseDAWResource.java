package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductResponseDAWResource implements Serializable {

	private static final long serialVersionUID = 2532423851764917600L;
	
	private ProductDAWResource product;

	public ProductResponseDAWResource() {}
	
	public ProductResponseDAWResource(ProductDAWResource product) {
		this.product = product;
	}

	public ProductDAWResource getProduct() {
		return product;
	}

	public void setProduct(ProductDAWResource product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ProductContainerResource [product=" + product + "]";
	}
	
}
