package com.xteam.discountasciiwarehouse.dao;

import java.io.IOException;

import com.xteam.discountasciiwarehouse.dao.exception.ProductNotFoundException;
import com.xteam.discountasciiwarehouse.domain.Product;

public interface ProductDao {

	Product getById(int id) throws ProductNotFoundException;

}
