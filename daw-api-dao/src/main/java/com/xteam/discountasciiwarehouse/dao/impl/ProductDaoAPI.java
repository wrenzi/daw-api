package com.xteam.discountasciiwarehouse.dao.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.xteam.discountasciiwarehouse.dao.ProductDao;
import com.xteam.discountasciiwarehouse.dao.exception.ProductNotFoundException;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductResponseDAWResource;
import com.xteam.discountasciiwarehouse.domain.Product;

@Repository("productDao")
public class ProductDaoAPI implements ProductDao {

	@Value("${daw.product.getbyid.url}")
	private String getById;
	
    @Autowired
    private DozerBeanMapper mapper;
    
    @Autowired
    private RestTemplate restTemplate;

	@Override
	@Cacheable("product")
	public Product getById(int productId) throws ProductNotFoundException {
		ProductDAWResource productSource = restTemplate.getForObject(this.getById, ProductResponseDAWResource.class, productId).getProduct();
		if (productSource==null) {
			throw new ProductNotFoundException();
		}
		return mapper.map(productSource, Product.class);
	}
	
}
