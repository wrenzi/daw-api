package com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.builder;

import java.util.Random;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.UserDAWResource;

public class UserDAWResourceBuilder {

	private String username;
	
	private String email;
	
    private UserDAWResourceBuilder() {}

	public static UserDAWResourceBuilder aUser() {
		UserDAWResourceBuilder builder = new UserDAWResourceBuilder();
		Random random = new Random();
		builder.username = "user" + random.nextInt(100);
		builder.email = "email" + random.nextInt(100) + "@domain.com";
		return builder;
	}
    
    public UserDAWResourceBuilder withUsername(String username) {
        this.username = username;
        return this;
    }

    public UserDAWResourceBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserDAWResource build() {
        return new UserDAWResource(this.username,this.email);
    }

}