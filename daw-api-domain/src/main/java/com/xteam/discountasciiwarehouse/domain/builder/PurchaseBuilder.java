package com.xteam.discountasciiwarehouse.domain.builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;

public class PurchaseBuilder {

	private Integer id; 
	
	private User user;
	
	private Product product;
	
	private Date date;
	
	public static PurchaseBuilder aPurchase() {
		Random rand = new Random();
		PurchaseBuilder builder = new PurchaseBuilder();
		builder
			.withId(rand.nextInt(100)+1)
			.withUser(UserBuilder.aUser().build())
			.withProduct(ProductBuilder.aProduct().build())
			.withDate(Calendar.getInstance().getTime());
		return builder;
	}
	
	public static List<Purchase> aList(int size) {
		List<Purchase> purchases = new ArrayList<Purchase>();
		for (int i=0; i<=size; i++) {
			purchases.add(PurchaseBuilder.aPurchase().build());
		}
		return purchases;
	}
	
	public PurchaseBuilder withId(Integer id) {
		this.id = id;
		return this;
	}

	public PurchaseBuilder withUser(User user) {
		this.user = user;
		return this;
	}
	
	public PurchaseBuilder withProduct(Product product) {
		this.product = product;
		return this;
	}

	public PurchaseBuilder withDate(Date date) {
		this.date = date;
		return this;
	}
	
	public Purchase build() {
		return new Purchase(this.id, this.user, this.product, this.date);
	}
		
}
