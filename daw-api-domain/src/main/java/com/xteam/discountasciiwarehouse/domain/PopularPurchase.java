package com.xteam.discountasciiwarehouse.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class PopularPurchase implements Serializable {

	private static final long serialVersionUID = -7639467362102358815L;

	private Integer id;
	
	private Product product;
	
	private Set<User> users;
	
	private Date date;

	public PopularPurchase() {}

	public PopularPurchase(Integer id, Product product, Set<User> users, Date date) {
		super();
		this.id = id;
		this.product = product;
		this.users = users;
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
