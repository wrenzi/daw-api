package com.xteam.discountasciiwarehouse.domain.builder;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.xteam.discountasciiwarehouse.domain.User;

public class UserBuilder {

	private String username;
	
	private String email;
	
	public static UserBuilder aUser() {
		Random rand = new Random();
		UserBuilder builder = new UserBuilder();		
		builder
			.withUsername("X User" + rand.nextInt())
			.withEmail("xuser" + rand.nextInt()+"@xuser.com");		
		return builder;
	}
	
	public static Set<User> aSet(int size) {
		Set<User> users = new HashSet<User>();
		for (int i=0; i<=size; i++) {
			users.add(UserBuilder.aUser().build());
		}
		return users;
	}
	
	public UserBuilder withUsername(String username) {
		this.username = username;
		return this;
	}

	public UserBuilder withEmail(String email) {
		this.email = email;
		return this;
	}

	public User build() {
		return new User(this.username, this.email);
	}
		
}
