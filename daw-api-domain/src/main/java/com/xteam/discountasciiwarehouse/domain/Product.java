package com.xteam.discountasciiwarehouse.domain;

import java.io.Serializable;

public class Product implements Serializable {

	private static final long serialVersionUID = 4259175116556887041L;

	private Integer id; 
	
	private String face;
	
	private Integer price; 

	private Integer size;
	
	public Product() {}
	
	public Product(Integer id, String face, Integer price, Integer size) {
		super();
		this.id = id;
		this.face = face;
		this.price = price;
		this.size = size;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	
	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", face=" + face + ", price=" + price + ", size=" + size + "]";
	}
	
}
