package com.xteam.discountasciiwarehouse.domain.builder;

import java.util.Random;

import com.xteam.discountasciiwarehouse.domain.Product;

public class ProductBuilder {

	private Integer id; 
	
	private String face;
	
	private Integer price; 

	private Integer size;

	public static ProductBuilder aProduct() {
		Random rand = new Random();
		ProductBuilder builder = new ProductBuilder();
		builder
			.withId(rand.nextInt(100))
			.withFace(":)")
			.withPrice(rand.nextInt(200)+1)
			.withSize(rand.nextInt(40)+1);
		return builder;
	}
	
	public ProductBuilder withId(Integer id) {
		this.id = id;
		return this;
	}

	public ProductBuilder withFace(String face) {
		this.face = face;
		return this;
	}

	public ProductBuilder withPrice(Integer price) {
		this.price = price;
		return this;
	}

	public ProductBuilder withSize(Integer size) {
		this.size = size;
		return this;
	}

	public Product build() {
		return new Product(this.id, this.face, this.price, this.size);
	}
		
}
