package com.xteam.discountasciiwarehouse.domain;

import java.io.Serializable;
import java.util.Date;

public class Purchase implements Serializable {

	private static final long serialVersionUID = -8323105264812657069L;

	private Integer id; 
	
	private User user;
	
	private Product product;
	
	private Date date;

	public Purchase() {
		super();
	}

	public Purchase(Integer id, User user, Product product, Date date) {
		super();
		this.id = id;
		this.user = user;
		this.product = product;
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
