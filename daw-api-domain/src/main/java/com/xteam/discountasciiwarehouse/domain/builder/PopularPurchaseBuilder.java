package com.xteam.discountasciiwarehouse.domain.builder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.xteam.discountasciiwarehouse.domain.PopularPurchase;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.User;

public class PopularPurchaseBuilder {

	private Integer id;
	
	private Product product;
	
	private Set<User> users;
	
	private Date date;
	
	public static PopularPurchaseBuilder aPopularPurchase() {
		Random rand = new Random();
		PopularPurchaseBuilder builder = new PopularPurchaseBuilder();
		builder
			.withId(rand.nextInt(100)+1)
			.withUsers(UserBuilder.aSet(4))
			.withProduct(ProductBuilder.aProduct().build())
			.withDate(Calendar.getInstance().getTime());
		return builder;
	}
	
	public static List<PopularPurchase> aList(int size) {
		List<PopularPurchase> purchases = new ArrayList<PopularPurchase>();
		for (int i=0; i<=size; i++) {
			purchases.add(PopularPurchaseBuilder.aPopularPurchase().build());
		}
		return purchases;
	}	
	
	public PopularPurchaseBuilder withId(Integer id) {
		this.id = id;
		return this;
	}

	public PopularPurchaseBuilder withUsers(Set<User> users) {
		this.users = users;
		return this;
	}
	
	public PopularPurchaseBuilder withProduct(Product product) {
		this.product = product;
		return this;
	}

	public PopularPurchaseBuilder withDate(Date date) {
		this.date = date;
		return this;
	}

	public PopularPurchase build() {
		return new PopularPurchase(this.id, this.product, this.users, this.date);
	}
		
}
