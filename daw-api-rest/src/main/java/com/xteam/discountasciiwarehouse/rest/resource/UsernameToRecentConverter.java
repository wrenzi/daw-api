package com.xteam.discountasciiwarehouse.rest.resource;

import java.util.HashSet;
import java.util.Set;

import org.dozer.DozerConverter;

import com.xteam.discountasciiwarehouse.domain.User;

@SuppressWarnings("rawtypes")
public class UsernameToRecentConverter extends DozerConverter<Set,Set> {

	public UsernameToRecentConverter() {
		super(Set.class, Set.class);
	}

	@Override
    public Set<String> convertTo(Set source, Set destination) {
		return null;
    }

    @Override
	@SuppressWarnings("unchecked")
    public Set<User> convertFrom(Set source, Set destination) {
    	destination = new HashSet<>();
    	for (Object user : source) {
    		destination.add(((User)user).getUsername());
    	}
    	return destination;
    }
    
}