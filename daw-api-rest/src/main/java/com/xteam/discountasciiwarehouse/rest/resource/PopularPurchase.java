package com.xteam.discountasciiwarehouse.rest.resource;

import java.io.Serializable;
import java.util.Set;

public class PopularPurchase implements Serializable {

	private static final long serialVersionUID = 8925589807194033430L;

	private int id;

	private String face;

	private int price;

	private int size;

	private Set<String> recent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFace() {
		return face;
	}

	public void setFace(String face) {
		this.face = face;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Set<String> getRecent() {
		return recent;
	}

	public void setRecent(Set<String> recent) {
		this.recent = recent;
	}

}
