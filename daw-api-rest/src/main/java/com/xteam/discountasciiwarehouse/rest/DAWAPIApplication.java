package com.xteam.discountasciiwarehouse.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DAWAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DAWAPIApplication.class, args); 
	}
	
}
