package com.xteam.discountasciiwarehouse.rest.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xteam.discountasciiwarehouse.dao.exception.UserNotFoundException;
import com.xteam.discountasciiwarehouse.rest.resource.PopularPurchase;
import com.xteam.discountasciiwarehouse.service.PurchaseService;

@RestController
@RequestMapping("/recent_purchases")
@Validated
public class RecentPurchaseController {

	Logger logger = LoggerFactory.getLogger(RecentPurchaseController.class);

	@Autowired
	private PurchaseService purchaseService;

	@Autowired
	private DozerBeanMapper mapper;

	@Cacheable("listByUsername")
	@RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listByUsername(@PathVariable("username") String username,
			@RequestParam("limit") Integer limit) throws MappingException, UserNotFoundException, IOException {
		try {
			List<PopularPurchase> popular = purchaseService.listPopular(username, limit)
					.stream()
					.map(entity -> mapper.map(entity, PopularPurchase.class))
					.collect(Collectors.toList());
			return ResponseEntity.ok(popular);
		} catch (UserNotFoundException e) {
			logger.error(e.getMessage());
			Map<String, String> payload = new TreeMap<String, String>();
			payload.put("status", HttpStatus.NOT_FOUND.toString());
			payload.put("message", e.getMessage());
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(payload);
		} catch (Exception e) {
			throw new IOException("The resource is temporarily unavailable", e.getCause());
		}

	}

}