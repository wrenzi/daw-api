package com.xteam.discountasciiwarehouse.rest.config;

import static org.dozer.loader.api.FieldsMappingOptions.customConverter;

import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.ProductDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.PurchaseDAWResource;
import com.xteam.discountasciiwarehouse.dao.integration.dawpurchases.resource.UserDAWResource;
import com.xteam.discountasciiwarehouse.domain.Product;
import com.xteam.discountasciiwarehouse.domain.Purchase;
import com.xteam.discountasciiwarehouse.domain.User;
import com.xteam.discountasciiwarehouse.rest.resource.UsernameToRecentConverter;

@Configuration
public class RestDozerConfig {

    @Bean
    public BeanMappingBuilder beanMappingBuilder() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                mapping(ProductDAWResource.class, Product.class);
                mapping(PurchaseDAWResource.class, Purchase.class)
                	.fields("username", "user.username")
                	.fields("productId", "product.id");
                mapping(UserDAWResource.class, User.class);
                mapping(com.xteam.discountasciiwarehouse.domain.PopularPurchase.class, com.xteam.discountasciiwarehouse.rest.resource.PopularPurchase.class)
            		.fields("product.face", "face")
                	.fields("product.price", "price")
                	.fields("product.size", "size")
                	.fields("users", "recent", customConverter(UsernameToRecentConverter.class));
            }
        };
    }

    @Bean
    public DozerBeanMapper beanMapper() {
        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
        dozerBeanMapper.addMapping(beanMappingBuilder());
        return dozerBeanMapper;
    }

}
