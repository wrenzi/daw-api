package com.xteam.discountasciiwarehouse.rest.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.google.common.cache.CacheBuilder;

@Configuration
@EnableCaching
public class RestCacheConfig {

	@Bean
	@Primary
	public CacheManager cacheManager() {
	    GuavaCacheManager cacheManager = new GuavaCacheManager();
	    cacheManager.setCacheBuilder(
	        CacheBuilder
	        	.newBuilder()
	        	.expireAfterWrite(2, TimeUnit.MINUTES)
	        	.maximumSize(1000));
	    return cacheManager;
	}
	
}