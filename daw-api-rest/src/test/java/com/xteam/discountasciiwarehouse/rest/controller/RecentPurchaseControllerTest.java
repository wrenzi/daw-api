package com.xteam.discountasciiwarehouse.rest.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.xteam.discountasciiwarehouse.dao.config.DozerConfig;
import com.xteam.discountasciiwarehouse.dao.exception.UserNotFoundException;
import com.xteam.discountasciiwarehouse.domain.builder.PopularPurchaseBuilder;
import com.xteam.discountasciiwarehouse.service.PurchaseService;
import com.xteam.discountasciiwarehouse.service.impl.PurchaseServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RecentPurchaseControllerTest {

	@Mock
	private PurchaseService purchaseService = new PurchaseServiceImpl();

	@Mock
	private DozerBeanMapper mapper = new DozerConfig().beanMapper();

	@InjectMocks
	private RecentPurchaseController controller = new RecentPurchaseController();

	MockMvc mockMvc;
	
	@Test
	public void existentUser() throws Exception {
		when(purchaseService.listPopular("Kade6", 5)).thenReturn(PopularPurchaseBuilder.aList(4));
		standaloneSetup(controller)
			.build()
			.perform(
				get("/recent_purchases/Kade6?limit=5")
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isOk());
	}
	
	@Test
	public void nonExistentUser() throws Exception {
		String message = "User with username of 'nonexistent' was not found";
		when(purchaseService.listPopular("nonexistent", 5)).thenThrow(new UserNotFoundException(message));
		standaloneSetup(controller)
			.build()
			.perform(
				get("/recent_purchases/nonexistent?limit=5")
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isNotFound())
					.andExpect(MockMvcResultMatchers.jsonPath("$.message").value(message));
	}

}
